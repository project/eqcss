Introduction
============

Element queries are a new way of thinking about responsive web design where the
responsive conditions apply to elements on the page instead of the width or
height of the browser.

Unlike CSS @media queries, @element Queries are aware of more than just the
width and height of the browser, you can write responsive conditions for a
number of different situations like how many characters of text or child
elements an element contains.

Another concept that element queries brings to CSS is the idea of ‘scoping’ your
styles to one element in the same way that JavaScript functions define a new
scope for the variables they contain.

-- http://elementqueries.com/

Main Features
-------------

* Includes eqcss.js on all non-admin pages through the Libraries API.

Installation
============

Requirements
------------

* Libraries module (https://www.drupal.org/project/libraries)
* EQCSS Library (https://github.com/eqcss/eqcss)

# Install the libraries module.
# Add the EQCSS library to the sites/all/libraries folder.
  Note: A .make file is included to download the library to the right folder.
# Install the EQCSS module.

Maintainers
===========

* Craig Aschbrenner <https://www.drupal.org/user/246322>
