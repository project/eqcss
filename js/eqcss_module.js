/**
 * @file
 * EQCSS.js integration.
 */

(function ($, Drupal) {
  'use strict';

  Drupal.behaviors.eqcssApply = {
    attach: function (context, settings) {

      // @see http://elementqueries.com/#running
      // @todo is this necessary for ajax loaded content?
      EQCSS.apply();

    }
  };

}(jQuery, Drupal));
